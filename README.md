Steps for deploy:

1. build containers:
```
docker-compose up --build -d
```
2. php-fpm container:
```
docker exec -it -w /handler-leads  handler-php-fpm bash 
```
3. run composer in php-fpm container:
```
composer install
```

Start application command:
```
yii lead [-c=10 -q=queue]

Parameters:
-c - count leads (default 10)
-q - queue that you push leads in (default 'queue')
There is 3 queues: 'queue', 'queueLead1', 'queueLead2'
```

Additional description:

Changed separator for console controller command just for fun.
This example has the same commands:
```
yii queue/info
yii queue::info
```