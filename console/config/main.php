<?php

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\controllers',
    'as separator' => app\behaviors\ConsoleSeparator::class,
    'bootstrap' => [
        'queue',
        'queueLead1',
        'queueLead2',
        'log',
    ],
    'components' => [
        'queue' => [
            'class' => \yii\queue\redis\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => [
                'hostname' => 'redis',
                'port' => 6379,
                'database' => 0,
            ],
            'channel' => 'queue',
        ],
        'queueLead1' => [
            'class' => \yii\queue\redis\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => [
                'hostname' => 'redis',
                'port' => 6379,
                'database' => 0,
            ],
            'channel' => 'queueLead1',
        ],
        'queueLead2' => [
            'class' => \yii\queue\redis\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => [
                'hostname' => 'redis',
                'port' => 6379,
                'database' => 0,
            ],
            'channel' => 'queueLead2',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::class,
                    'exportInterval' => 1,
                    'logVars' => [],
                ],
            ],
        ],
    ],
];
