<?php declare(strict_types=1);

namespace app\services;

use app\jobs\ProcessLead;
use LeadGenerator\Generator;
use LeadGenerator\Lead;
use yii\queue\Queue;

class LeadService
{
    public function __construct(
        private readonly Generator $generator,
        private readonly Queue $queue,
    )
    {
    }

    /**
     * @param int $count
     * @return void
     */
    public function createLead(int $count): void
    {
        $this->generator->generateLeads($count, $this->getCallable());
    }

    private function getCallable(): callable
    {
        return function (Lead $lead) {
//            if (!$this->validator->validate($lead)) {
//                \Yii::error("handling the $lead->categoryName is not possible");
//                return;
//            }
            $this->queue
                ->delay(2)
                ->push(new ProcessLead(['lead' => $lead]));
        };
    }
}