<?php declare(strict_types=1);

namespace app\tests;

use app\jobs\ProcessLead;
use app\services\LeadService;
use LeadGenerator\Generator;
use yii\queue\redis\Queue;

class LeadServiceTest extends \Codeception\Test\Unit
{
    public function testCreateLeadEnqueuesExpectedNumberOfJobs()
    {
        // Create a mock Queue object
        $queueMock = $this->createMock(Queue::class);

        // Configure mock for one method call
        $queueMock->expects($this->once())
            ->method('push')
            ->with($this->isInstanceOf(ProcessLead::class));

        // Create a mock Generator object
        $generatorMock = $this->createMock(Generator::class);

        // Create a LeadService object with the mocks
        $service = new LeadService($queueMock, $generatorMock);

        // Call the createLead method with count equal to 1
        $service->createLead(1);
    }
}