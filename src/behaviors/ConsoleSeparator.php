<?php declare(strict_types=1);

namespace app\behaviors;

use yii\base\Application;
use yii\base\Behavior;
use yii\base\Event;

class ConsoleSeparator extends Behavior
{
    public function events(): array
    {
        return [
            Application::EVENT_BEFORE_REQUEST => 'beforeRequest',
        ];
    }

    public function beforeRequest(Event $event)
    {
        $request = $event->sender->getRequest();
        $params = $request->getParams();
        if ($params[0] !== null) {
            $params[0] = str_replace('::', '/', $params[0]);
            $request->setParams($params);
        }
    }
}