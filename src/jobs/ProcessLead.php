<?php declare(strict_types=1);

namespace app\jobs;

use LeadGenerator\Lead;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class ProcessLead extends BaseObject implements JobInterface
{
    public Lead $lead;

    public function getDate(): string
    {
        return date('d-m-Y H:i:s');
    }

    public function execute($queue)
    {
        file_put_contents(
            '/handler-leads/leads.txt',
            "{$this->lead->id} | {$this->lead->categoryName} | {$this->getDate()}\n",
            FILE_APPEND);
    }
}