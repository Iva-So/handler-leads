<?php declare(strict_types=1);

namespace app\dto;

use LeadGenerator\Lead;

class LeadDto
{
    /**
     * @param Lead $lead
     * @param int $id
     * @param string $categoryName
     */
    public function __construct(
        Lead             $lead,
        protected int    $id,
        protected string $categoryName,
    )
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return LeadDto
     */
    public function setId(int $id): LeadDto
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    /**
     * @param string $categoryName
     * @return LeadDto
     */
    public function setCategoryName(string $categoryName): LeadDto
    {
        $this->categoryName = $categoryName;
        return $this;
    }
}