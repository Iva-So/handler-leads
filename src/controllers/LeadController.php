<?php declare(strict_types=1);

namespace app\controllers;

use app\services\LeadService;
use LeadGenerator\Generator;
use yii\base\InvalidConfigException;
use yii\console\Controller;

class LeadController extends Controller
{
    public $count = 10;
    public $queue = 'queue';

    public function options($actionID): array
    {
        return
            [
                'count',
                'queue',
            ];
    }

    public function optionAliases(): array
    {
        return
            [
                'c' => 'count',
                'q' => 'queue'
            ];
    }

    /**
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $queue = \Yii::$app->get($this->queue);
        $leadService = new LeadService(new Generator(), $queue);
        $leadService->createLead($this->count);
    }
}